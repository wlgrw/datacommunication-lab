# Datacommunication Lab (DCML)

Code base for DCML with inspiration for students to develop their own test software.

The code examples are primary for use on the windows platform but a reference is also provided for Linux.

## Windows 
These code examples are taken from:
- https://docs.microsoft.com/en-us/windows/win32/winsock/complete-client-code
- https://docs.microsoft.com/en-us/windows/win32/winsock/complete-server-code

The examples for Windows are preconfigured for use with QT Creator.

## Linux
When using Linux this page is a good source: http://www.linuxhowtos.org/C_C++/socket.htm

# Disclaimer
This repository is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; Without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# License

Linux howtos are copyrighted by Sascha Nitsch Unternehmensberatung GmbH.

Winsock client and server example are copyrighted by Microsoft.

All other documents are free: You can redistribute it and/or modify it under the terms of a Creative Commons Attribution-NonCommercial 4.0 International License (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.


