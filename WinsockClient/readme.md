# WinsockClient Example
This example can be used to create test software to analyse the interaction between client and server
and to perform time measurements.

## source
This example was taken from
- https://docs.microsoft.com/en-us/windows/win32/winsock/complete-client-code

## Use with QT Creator
This code is intened for MS-Visualstudio-IDE but can also be used in combination Qt when adapted for this:
1. The lines with #pragma shall be removed from the .pro Qt-projectfile .
2. In the pro Qt-projectfile the following line shall be added to use the correct library:
```
LIBS += -lws2_32
```

The Qt development environment also offers the possibility to use sockets with the help of the QTcpSocket class.
