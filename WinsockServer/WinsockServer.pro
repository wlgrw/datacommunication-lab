TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        WinsockServerSourceCode.c

LIBS += -lws2_32

DISTFILES += \
    readme.md
